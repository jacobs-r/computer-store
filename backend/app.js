var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('dotenv').config();
//Cross-origin resource sharing (CORS) allows AJAX requests 
//to skip the Same-origin policy and access resources from remote hosts.
var cors = require('cors');

global.conn = require('./dbconnection/dbconnect');

conn.connect((err) => {
  if (err) throw err;
  console.log('Connected to MySQL');
});

var customersRouter = require('./routes/customers')
var departmentsRouter = require('./routes/departments');
var employeesRouter = require('./routes/employees');
var suppliersRouter = require('./routes/suppliers');
var shippersRouter = require('./routes/shippers');
var productsRouter = require('./routes/products');
var ordersRouter = require('./routes/orders');

var app = express();
app.use(cors()); //Allows CORS

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/customers', customersRouter);
app.use('/departments', departmentsRouter);
app.use('/employees', employeesRouter);
app.use('/suppliers', suppliersRouter);
app.use('/shippers', shippersRouter);
app.use('/products', productsRouter);
app.use('/orders', ordersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
