var express = require('express');
var router = express.Router();

router.get('/get', (req, res) => {
  let sqlCmd = 'SELECT * FROM customers';
  conn.query(sqlCmd, (err, result) => {
    if (err) throw err;
    res.end(JSON.stringify(result));
  })
});

router.post('/save', (req, res) => {
  let form = req.body;
  let sqlCmd = 'INSERT INTO customers SET ?';
  conn.query(sqlCmd, form, (err, result) => {
    if (err) throw err;
    res.end(JSON.stringify(result.insertId)); //Send the ID of the last record inserted
  })
});

router.delete('/delete/:custId', (req, res) => {
  let custId = req.params.custId;
  let sqlCmd = 'DELETE FROM customers WHERE custId = ?';
  conn.query(sqlCmd, custId, (err, result) => {
    if (err) throw err;
    res.end();
  });
});

router.put('/update/:id', (req, res) => {
  let custId = req.params.id;
  let form = req.body;
  let sqlCmd = 'UPDATE customers SET ? WHERE custId = ?';
  conn.query(sqlCmd, [form, custId], (err, result) => {
    if (err) throw err;
    res.end();
  });
});
module.exports = router;
