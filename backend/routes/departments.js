var express = require('express');
var router = express.Router();

router.get('/get', (req, res) => {
  let sqlCmd = 'SELECT * FROM departments';
  conn.query(sqlCmd, (err, result) => {
    if (err) throw err;
    res.end(JSON.stringify(result));
  })
});

router.post('/save', (req, res) => {
  let form = req.body;
  let sqlCmd = 'INSERT INTO departments SET ?';
  conn.query(sqlCmd, form, (err, result) => {
    if (err) throw err;
    res.end(JSON.stringify(result.insertId)); //Send the ID of the last record inserted
  })
});

router.delete('/delete/:deptid', (req, res) => {
  let deptid = req.params.deptid;
  let sqlCmd = 'DELETE FROM departments WHERE deptid = ?';
  conn.query(sqlCmd, deptid, (err, result) => {
    if (err) throw err;
    res.end();
  });
});

router.put('/update/:id', (req, res) => {
  let deptid = req.params.id;
  let form = req.body;
  let sqlCmd = 'UPDATE departments SET ? WHERE deptid = ?';
  conn.query(sqlCmd, [form, deptid], (err, result) => {
    if (err) throw err;
    res.end();
  });
});
module.exports = router;
