const express = require('express');
const router = express.Router();
//const faker = require('faker');

// let ids = [];
// router.get('/', (req, res) => {
//     let getDeptIDCmd = 'SELECT deptid FROM departments ORDER BY rand() limit 1';
//     for (let i = 0; i < 15; i++) {
//         conn.query(getDeptIDCmd, (err, result) => {
//             if (err) throw err;
//             ids.push(result[0].deptid);
//             return res.end();
//         });
//     }
// });

// router.get('/fill', (req, res) => {
//     let sqlCmd = 'INSERT INTO employees (employname, employaddress, employphone, deptid) VALUES ?';
//     let values = [];
//     for (let i = 0; i < 15; i++) {
//         values.push([faker.name.findName(), faker.address.streetAddress(), faker.phone.phoneNumber('239#######'), ids[i]]);
//     }
//     conn.query(sqlCmd, [values], (err, result) => {
//         if (err) throw err;
//         return res.end();
//     });
// });

router.get('/get', (req, res) => {
    let query = `SELECT * FROM employees`
    conn.query(query, (err, result) => {
        if (err) throw err;
         res.send({
            employees: JSON.stringify(result)
         });
        res.end();
    })
});

router.post('/save', (req, res) => {
    let form = req.body;
    let sqlCmd = 'INSERT INTO employees SET ?';
    conn.query(sqlCmd, form, (err, result) => {
        if (err) throw err;
        res.send(JSON.stringify(result.insertId));
        res.end();
    });
});

router.delete('/delete/:id', (req, res) => {
    let employId = req.params.id;
    let sqlCmd = 'DELETE FROM employees WHERE employId = ?';
    conn.query(sqlCmd, employId, (err, result) => {
        if (err) throw err;
        res.end();
    });
});

router.put('/update/:id', (req, res) => {
    let employId = req.params.id;
    let form = req.body;
    let sqlCmd = 'UPDATE employees SET ? WHERE employId = ?';
    conn.query(sqlCmd, [form, employId], (err, result) => {
        if (err) throw err;
        res.end();
    });
});

module.exports = router;