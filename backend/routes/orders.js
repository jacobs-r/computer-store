const express = require('express');
const router = express.Router();

router.get('/get', (req, res) => {
    let queries = [
        'SELECT * FROM suppliers',
        'SELECT * FROM customers',
        'SELECT * FROM employees',
        `SELECT orders.*, suppliers.suppId, suppName, customers.custId, custName, custLastName, employees.employId, employLastName 
         FROM orders INNER JOIN suppliers 
         ON orders.suppId = suppliers.suppId
         INNER JOIN customers
         ON orders.custId = customers.custId
         INNER JOIN employees
         ON orders.employId = employees.employId`
    ];
    conn.query(queries.join(';'), (err, result) => {
        if (err) throw err;
        res.send({
            suppliers: JSON.stringify(result[0]),
            customers: JSON.stringify(result[1]),
            employees: JSON.stringify(result[2]),
            orders: JSON.stringify(result[3]),
        });
        res.end();
    })
});

router.post('/save', (req, res) => {
    let form = req.body;
    let sqlCmd = 'INSERT INTO orders SET ?';
    conn.query(sqlCmd, form, (err, result) => {
        if (err) throw err;
        res.send(JSON.stringify(result.insertId));
        res.end();
    });
});

router.delete('/delete/:id', (req, res) => {
    let orderId = req.params.id;
    let sqlCmd = 'DELETE FROM orders WHERE orderId = ?';
    conn.query(sqlCmd, orderId, (err, result) => {
        if (err) throw err;
        res.end();
    });
});

router.put('/update/:id', (req, res) => {
    let orderId = req.params.id;
    let form = req.body;
    let sqlCmd = 'UPDATE orders SET ? WHERE orderId = ?';
    conn.query(sqlCmd, [form, orderId], (err, result) => {
        if (err) throw err;
        res.end();
    });
});

module.exports = router;