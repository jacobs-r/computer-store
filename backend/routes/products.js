const express = require('express');
const router = express.Router();

router.get('/get', (req, res) => {
    let queries = [
        'SELECT * FROM suppliers',
        `SELECT products.*, suppliers.suppId, suppName 
         FROM products INNER JOIN suppliers 
         ON products.suppId = suppliers.suppId`
    ];
    conn.query(queries.join(';'), (err, result) => {
        if (err) throw err;
         res.send({
            suppliers: JSON.stringify(result[0]),
            products: JSON.stringify(result[1])
         });
        res.end();
    })
});

router.post('/save', (req, res) => {
    let form = req.body;
    let sqlCmd = 'INSERT INTO products SET ?';
    conn.query(sqlCmd, form, (err, result) => {
        if (err) throw err;
        res.send(JSON.stringify(result.insertId));
        res.end();
    });
});

router.delete('/delete/:id', (req, res) => {
    let productId = req.params.id;
    let sqlCmd = 'DELETE FROM products WHERE productId = ?';
    conn.query(sqlCmd, productId, (err, result) => {
        if (err) throw err;
        res.end();
    });
});

router.put('/update/:id', (req, res) => {
    let productId = req.params.id;
    let form = req.body;
    let sqlCmd = 'UPDATE products SET ? WHERE productId = ?';
    conn.query(sqlCmd, [form, productId], (err, result) => {
        if (err) throw err;
        res.end();
    });
});

module.exports = router;