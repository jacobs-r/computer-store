const express = require('express');
const router = express.Router();

router.get('/get', (req, res) => {
    let query = `SELECT * FROM shippers`
    conn.query(query, (err, result) => {
        if (err) throw err;
         res.send({
            shippers: JSON.stringify(result)
         });
        res.end();
    })
});

router.post('/save', (req, res) => {
    let form = req.body;
    let sqlCmd = 'INSERT INTO shippers SET ?';
    conn.query(sqlCmd, form, (err, result) => {
        if (err) throw err;
        res.send(JSON.stringify(result.insertId));
        res.end();
    });
});

router.delete('/delete/:id', (req, res) => {
    let shipId = req.params.id;
    let sqlCmd = 'DELETE FROM shippers WHERE shipId = ?';
    conn.query(sqlCmd, shipId, (err, result) => {
        if (err) throw err;
        res.end();
    });
});

router.put('/update/:id', (req, res) => {
    let shipId = req.params.id;
    let form = req.body;
    let sqlCmd = 'UPDATE shippers SET ? WHERE shipId = ?';
    conn.query(sqlCmd, [form, shipId], (err, result) => {
        if (err) throw err;
        res.end();
    });
});

module.exports = router;