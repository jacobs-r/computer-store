const express = require('express');
const router = express.Router();

router.get('/get', (req, res) => {
    let query = `SELECT * FROM suppliers`
    conn.query(query, (err, result) => {
        if (err) throw err;
         res.send({
            suppliers: JSON.stringify(result)
         });
        res.end();
    })
});

router.post('/save', (req, res) => {
    let form = req.body;
    let sqlCmd = 'INSERT INTO suppliers SET ?';
    conn.query(sqlCmd, form, (err, result) => {
        if (err) throw err;
        res.send(JSON.stringify(result.insertId));
        res.end();
    });
});

router.delete('/delete/:id', (req, res) => {
    let suppId = req.params.id;
    let sqlCmd = 'DELETE FROM suppliers WHERE suppId = ?';
    conn.query(sqlCmd, suppId, (err, result) => {
        if (err) throw err;
        res.end();
    });
});

router.put('/update/:id', (req, res) => {
    let suppId = req.params.id;
    let form = req.body;
    let sqlCmd = 'UPDATE suppliers SET ? WHERE suppId = ?';
    conn.query(sqlCmd, [form, suppId], (err, result) => {
        if (err) throw err;
        res.end();
    });
});

module.exports = router;