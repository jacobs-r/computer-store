-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: computer-store
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customers` (
  `custId` int NOT NULL AUTO_INCREMENT,
  `custName` varchar(45) NOT NULL,
  `custLastName` varchar(45) NOT NULL,
  `custAddress` varchar(45) NOT NULL,
  `custCity` varchar(45) NOT NULL,
  `custZip` varchar(45) NOT NULL,
  `custCountry` varchar(45) NOT NULL,
  `custPhone` varchar(45) NOT NULL,
  PRIMARY KEY (`custId`),
  UNIQUE KEY `custId_UNIQUE` (`custId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'John','Tannon','187 Amerige Dr.','Greensboro','27405','United States','526-657-7782'),(2,'Raymond','Duarte','466 Virginia Ave.','Lima','11776','Peru','859-674-8999'),(3,'Marissa','Bartlett','679 New Ave.','Curitiba','11102','Brazil','652-588-7100'),(4,'Kianna','Hoover','25 East Glen Ridge Drive','Hangzhou','60073','China','251-741-9993'),(5,'Camryn','Reed','7717 Bow Ridge Avenue','Hefei','35007','China','128-325-5798'),(6,'Aditya','Stephens','697 Old Pleasant Street','Barranquilla','14120','Colombia','365-744-2563'),(7,'Matias','Peters','955 Victoria Drive','Madrid','21030','Spain','624-785-1532'),(8,'Mark','Barber','907 West Blue Spring St.','Semarang','07860','Indonesia','263-986-5915');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employees` (
  `employId` int NOT NULL AUTO_INCREMENT,
  `employName` varchar(45) NOT NULL,
  `employLastName` varchar(45) NOT NULL,
  `employBday` varchar(45) NOT NULL,
  `employEmail` varchar(45) NOT NULL,
  PRIMARY KEY (`employId`),
  UNIQUE KEY `employId_UNIQUE` (`employId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,'Stacey','Nadel','01-06-1989','StaceyPNadel@dayrep.com'),(2,'Jeffery','Pearce','01-12-1952','Jeffery@Pearce@rhyta.com'),(3,'Tom','Clancy','06-23-1980','TomPClancy@dayrep.com'),(4,'Roberta','Hickson','07-25-1980','RobertaEHickson@dayrep.com'),(5,'Floyd','Thomas','02-28-1993','FloydLThomas@dayrep.com');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `orderId` int NOT NULL AUTO_INCREMENT,
  `orderDate` varchar(45) NOT NULL,
  `suppId` int NOT NULL,
  `custId` int NOT NULL,
  `employId` int NOT NULL,
  PRIMARY KEY (`orderId`),
  UNIQUE KEY `orderId_UNIQUE` (`orderId`),
  KEY `order-supplier_idx` (`suppId`),
  KEY `customer-that-ordered_idx` (`custId`),
  KEY `employee-who-serviced-order_idx` (`employId`),
  CONSTRAINT `customer-that-ordered` FOREIGN KEY (`custId`) REFERENCES `customers` (`custId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee-who-serviced-order` FOREIGN KEY (`employId`) REFERENCES `employees` (`employId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order-supplier` FOREIGN KEY (`suppId`) REFERENCES `suppliers` (`suppId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'01-02-2020',1,3,2),(2,'03-05-2020',4,4,3),(3,'09-01-2019',1,1,1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `productId` int NOT NULL AUTO_INCREMENT,
  `productName` varchar(45) NOT NULL,
  `productUnit` varchar(45) NOT NULL,
  `productPrice` decimal(10,0) NOT NULL,
  `productCategory` varchar(45) NOT NULL,
  `suppId` int NOT NULL,
  PRIMARY KEY (`productId`),
  UNIQUE KEY `productId_UNIQUE` (`productId`),
  KEY `supplier-of-products_idx` (`suppId`),
  CONSTRAINT `supplier-of-products` FOREIGN KEY (`suppId`) REFERENCES `suppliers` (`suppId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Apple laptop','45A',300,'computers',1),(2,'Unassembled PCB','64B',20,'parts',1),(3,'Mouse','12K',10,'accessories',5),(4,'R.C. Car','95G',46,'toys',3),(5,'1tb SSD','01E',400,'computers',3),(6,'Raspberry Pi 4','64F',35,'computers',3),(7,'Usb-a to Usb-c cable','34I',5,'Accessories',4);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shippers`
--

DROP TABLE IF EXISTS `shippers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shippers` (
  `shipId` int NOT NULL AUTO_INCREMENT,
  `shipName` varchar(45) NOT NULL,
  `shipPhone` varchar(45) NOT NULL,
  PRIMARY KEY (`shipId`),
  UNIQUE KEY `shipId_UNIQUE` (`shipId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shippers`
--

LOCK TABLES `shippers` WRITE;
/*!40000 ALTER TABLE `shippers` DISABLE KEYS */;
INSERT INTO `shippers` VALUES (1,'Mary\'s Movers','652-577-8711'),(2,'LightSpeed','232-652-3222'),(3,'X-Press','874-989-3205');
/*!40000 ALTER TABLE `shippers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `suppliers` (
  `suppId` int NOT NULL AUTO_INCREMENT,
  `suppName` varchar(45) NOT NULL,
  `suppAddress` varchar(45) NOT NULL,
  `suppCity` varchar(45) NOT NULL,
  `suppZip` varchar(45) NOT NULL,
  `suppCountry` varchar(45) NOT NULL,
  `suppPhone` varchar(45) NOT NULL,
  PRIMARY KEY (`suppId`),
  UNIQUE KEY `suppId_UNIQUE` (`suppId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'John\'s Warehouse','984 Oak Meadow Ave.','Cranford','07016','Bolivia','615-784-7877'),(2,'Matt\'s Marvelous Manor','38 Brickwell Street','Levittown','11756','Bulgaria','251-784-6324'),(3,'Wonderhouse','65 West Green Hill lane','Lagrange','30240','Moldova','454-855-6232'),(4,'Towering Munitions','710 Elmwood Ave.','Oviedo','32765','Finland','219-892-8888'),(5,'ACME','74 Center St.','Sumter','29150','New Zealand','211-333-6242');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-23 10:10:39
