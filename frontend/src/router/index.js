import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Departments from '../views/Departments.vue'
import Employees from '../views/Employees.vue'
import Customers from '../views/Customers.vue'
import Suppliers from '../views/Suppliers.vue'
import Shippers from '../views/Shippers.vue'
import Products from '../views/Products.vue'
import Orders from '../views/Orders.vue'

//Used to define the base URL of all AJAX requests to the backend
//This is the URL where the Express server is listening for requests
//Made global to be used by the whole application
global.baseURL = 'http://localhost:3000';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/customers',
    name: 'Customers',
    component: Customers
  },
  {
    path: '/departments',
    name: 'Departments',
    component: Departments
  },
  {
    path: '/employees',
    name: 'Employees',
    component: Employees
  },
  {
    path: '/suppliers',
    name: 'Suppliers',
    component: Suppliers
  },
  {
    path: '/shippers',
    name: 'Shippers',
    component: Shippers
  },
  {
    path: '/products',
    name: 'Products',
    component: Products
  },
  {
    path: '/orders',
    name: 'Orders',
    component: Orders,
  }
]

const router = new VueRouter({
  routes
})

export default router

